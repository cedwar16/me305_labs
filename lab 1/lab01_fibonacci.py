'''@file lab01_fibonacci.py
There must be a docstring at the beginning of a python source file
with an \@file [filename] tag in it!
'''

#Function Definition
def fib(idx):
   
    a = 0
    b = 1
     
    if idx < 0:
        print("Please only input a positive Integer!")

    elif idx == 0:
        return a

    elif idx == 1:
        return b
    else:
        return fib(idx-1) + fib(idx-2)
        
        
    '''
    @brief      This function calculates a Fibonacci number at a speciic index.
    @param idx  An integer specifying the index of the desired
                Fibonacci number
    '''
    return 0


if __name__ == '__main__':

    while True:
        try:
            
            idx = int(input('Please input a positive integer to get a Fibonacci number: '))
            
            if idx<0:
                fib(idx)
            
            elif idx==0:
                print('0')
            
            elif idx==1:
                print('1')
                
            else:
                print(fib(idx))
                
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(true) loop when desired
            print('Program ended, see you soon!')
            break