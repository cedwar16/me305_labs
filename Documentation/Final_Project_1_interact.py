import serial

def getData():
    
    ser.write('g'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout+=1
        if timeout > 1_000_000:
            return None
    return ser.readline().decode()

# The with block here will automatically close the serial port once the block 
# ends you could just make a serial object as usual instead
with serial.Serial(port='COM4',baudrate=115273,timeout=1) as ser:
    while True:
        user_in = input("Enter 'g' to get data from the Nucleo or 's' to stop\r\n>>")
        if user_in == 'g' or user_in == 'G':
            dataString = getData()
            print("The string is: " + dataString)
            
            # Remove line endings
            strippedString = dataString.strip()
            
            # split on the commas
            splitStrings = dataString.split(',')
            
            print("The floating point values are: ")
            print(float(splitStrings[0]))
            print("and")
            print(float(splitStrings[1]))
            
        elif user_in == 's' or user_in == 'S':
            print('Thanks')
            break