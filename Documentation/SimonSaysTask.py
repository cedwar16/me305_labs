import pyb
import time
import os
import sys
import random

class simonSays:
    
    
    
    def __init__(self):
        
        self.state = 0
        
    def run(self):
        
        pinC13 = pyb.Pin (pyb.Pin.cpu.C13, pyb.Pin.OUT_PP)
        LED_blink = (100, 1000)
        simon_question_total = 5
        
            # main program code goes here
        if self.state==0:
            # run state 0 (init) code
            print('Welcome to Simon Says! 5 questions total, follow the pattern as shown by the LED')
            if self.myCallback():
                ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=myCallback)
                self.LED_pattern('start') # Command motor to go forward
                self.state = 1 # Updating state for next iteration
            
        elif self.state==1:
            # run state 1 (Running LED pattern) code
            # Code the light pattern here
                self.state = 2
                
        elif self.state==2:
            # run state 2 (LED pattern stop) code
            time.sleep(3)
            os.system('cls')
            self.state = 3
                    
        elif self.state==3:
            # run state 3 (ask the player to input the pattern) code
            for num in simon_says_list:
                player_answer = input('Please press the button as the pattern shown')
                self.state = 4
                
        elif self.state==4:
            # run state 4 (evaluating the input) code
            if player_answer == num:
                self.state = 7
            
            else:
                self.state = 5
            
        elif self.state==7:
            # tun state 7 (congratulate the player) if the player's anwer match the pattern
            print('Well Done!')
            self.state = 1
            
        elif self.state==5:
            print('Your answer does not match the pattern. GAME OVER!')
            print('Your score is: (0)'.format(question))
            sys.exit()
                
        else:
            pass
        # code to run if state number is invalid
        # program should ideally never reach here
        

    def ButtonInt(self):
        return random.choice([True, False]) # randomly return T or F
