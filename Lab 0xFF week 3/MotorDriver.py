## @file MotorDriver.py
#  This file serves as the Motor Driver Class and will set up our Model DRV8847
#  DC Motor. It will also be able to enable, disable, and set the duty cycle of
#  the motor.
#  
#
#  @author Christopher Edward



global fault
## Global variable that will detect the fault
fault = False

import pyb

## This class allows us to utilize our motors for the term project
class MotorDriver:
    ''' This class implements a motor driver for the ME405 term project. '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, motorset, nFAULT_pin): 
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param motorset     A pyb.Timer object to use for PWM generation on 
                            IN1_pin and IN2_pin.
	    @param nFAULT_pin   A pyb.Pin object use for the Fault indicator pin
        '''
        
        ## The nSLEEP pin set up and started 'off' or 'low'
        self.pinnSleep = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinnSleep.low()
        
        ## The timer used for sending PWM Pulses to the Motor
        self.tim = pyb.Timer(3, freq=20000)
        
        ## The IN1 pin setup
        self.in1 = pyb.Pin(IN1_pin)
        
        ## The IN2 pin setup
        self.in2 = pyb.Pin(IN2_pin)
        
        if motorset == 1:            
            ## The Channel Setup for IN1
            self.timch1 = self.tim.channel(1,pyb.Timer.PWM, pin=self.in1)
        
            ## The Channel Setup for IN2
            self.timch2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.in2)
        else:
            ## The Channel Setup for IN1
            self.timch1 = self.tim.channel(3,pyb.Timer.PWM, pin=self.in2)
        
            ## The Channel Setup for IN2
            self.timch2 = self.tim.channel(4, pyb.Timer.PWM, pin=self.in2)
        
        ## Inital Setup for the Motor to Spin Forward at 35% Duty
        self.common_duty = 40
        #self.timch1.pulse_width_percent(35)
        #self.timch2.pulse_width_percent(0)
        self.nFAULT_pin = nFAULT_pin
        print ('Creating a motor driver') 
	
        try:
            ## Creating external interrupt
            self.extint = pyb.ExtInt(self.nFAULT_pin, mode=pyb.ExtInt.IRQ_FALLING, 
    				     pull=pyb.Pin.PULL_UP, callback=self.nFAULT_pin)
        except:
            pass

    ## Turns the motor on at the specified speed
    def enable (self):
        '''
        @brief     This will activate both motors at the predetermined duty cycle
        '''
        self.pinnSleep.high()
        print ('Motor(s) Enabled') 
    
    ## Turns the motor off
    def disable (self):
        '''
        @brief     This will deactivate the motor, which will cause it to come to a stop
        '''
        self.timch1.pulse_width_percent(0)
        self.timch2.pulse_width_percent(0)
        print ('Motor Disabled') 
    
    def esc (self):
        '''
        @brief     This will deactivate all motors quickly, while retaining their values
        '''
        self.pinnSleep.low()
        print ('Motor(s) Disabled')
    
    def set_duty (self, duty):
        ''' 
        @brief       This will set the duty used by the motor
        @details     This method sets the duty cycle to be sent to the motor to the 
                     given level. Positive values cause effort in one direction, negative 
                     values in the opposite direction.
        @param duty  A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        dutyuse = abs(int(duty))  
        if duty == 0:
            self.disable()
        elif duty>0:
            self.timch1.pulse_width_percent(dutyuse)
            self.timch2.pulse_width_percent(0)
            self.enable()
        else:
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(int(dutyuse))
            self.enable()

    def nFAULT(self, pin):
        '''
        @brief When the external interrupt is triggered, this method will shut down
        the motors
        '''
        global fault
        fault = True
        self.disable()
        print('Yo this current is killing me. Im gonna die.')        

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. 
    # Any code within the if __name__ == '__main__' block will only run when 
    # the script is executed as a standalone program. If the script is 
    # imported as a module the code block will not run. Create the pin objects
    # used for interfacing with the motor driver
    
    ## A pyb.Pin object to use as the enable pin.
    pin_nSLEEP = pyb.Pin.cpu.A15
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 1
    pin_IN1 = pyb.Pin.cpu.B0
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 1
    pin_IN2 = pyb.Pin.cpu.B1
     
    ## A pyb.Pin object to use as the input to half bridge 1 for motor 2
    pin_IN12 = pyb.Pin.cpu.B4 
    ## A pyb.Pin object to use as the input to half bridge 2 for motor 2
    pin_IN22 = pyb.Pin.cpu.B5
    ## Create the timer channels for motor 1
    tim1 = 2
    ## Create the timer channels for motor 2
    tim2 = 1

    ## Create a motor 1 object passing in the pins and timer
    moet = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim1)
    ## Create a motor 2 object passing in the pins and timer
    moeb = MotorDriver(pin_nSLEEP, pin_IN12, pin_IN22, tim2)
    
    ## Enable motor 1 driver
    moet.enable()
    ## Enable motor 2 driver
    moeb.enable()
    
    ## Set the duty cycle of motor 1
    moet.set_duty(20)
    ## Set the duty cycle of motor 2
    moeb.set_duty(50)