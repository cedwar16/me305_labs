# -*- coding: utf-8 -*-
"""
Created on Wed Feb 25 23:39:49 2021

@author: chr15
"""

from matplotlib import pyplot
from array import array
from math import exp, sin, pi
from pyb import UART
import utime
import keyboard

if __name__ == "__main__":
    state = 0
    myuart = UART(2)
    n = 0
    t = 0
    time = array('f', list(t/10 for t in range(3001)))
    values = array('f', 3001*[])
    
    if state == 0:
        print('state 0 =  start')
    while True:
        start = utime.ticks_ms()
        if state == 0:
            if myuart.anu():
                val = myuart.readchar()
                if val == 103:
                    myuart.write('ASCII '+ str(val) + 'was sent to the Nucleo')
                    state = 1
                    t1 = utime.ticks_ms()
                    
        elif state == 1:
            t2 = utime.ticks_diff(start, t1)/1000
            print('state 1 = data collection')
            values.append(exp(-time[n]/10)*sin(2*pi/3*time[n]))
            print('n = ' +str(n))
            print('t1 = ' +str(t1))
            print('t2= ' +str(t2))
            print('mod t2 = ' +str(t2%1))
            if t2 % 1 > 0.1:
                n += 1
            if t2 == 30:
                state = 2
            if myuart.any():
                val = myuart.readchar()
                if val == 115:
                    myuart.write('ASCII ' + str(val) + 'was sent to the Nucleo')
                    state = 2
        
        elif state == 2:
            print('state 2 = printing data')
            myuart.write('{:}, {:}'.format(time[n], values[n]))
            pyplot.figure()
            pyplot.plot(time, values)
            pyplot.xlabel('Time')
            pyplot.ylabel('Data')

            # As an example, print the data as a CSV to the console
            for n in range(len(time)):
                print('{:}, {:}'.format(time[n], values[n]))
                # print(str(times[n]) + ', ' + str(values[n]))
                
            # As another example, build a single Line string, strip special characters,
            # split on the commas, then float the entries
            num1 = 1
            num2 = 2
            # Generate string (you will rerad this with ser.read() in your PC script)
            myLineString = '{:}, {:}\r\n'.format(num1, num2)
            
            # Remove line endings
            myStrippedString = myLineString.strip()
            
            # Split on the commas
            mySplitStrings = myStrippedString.split(',')
            
            # Convert entries to numbers from strings
            myNum1 = float(mySplitStrings[0])
            print(myNum1)
            myNum2 = float(mySplitStrings[1])
            print(myNum2)
            
        else:
            pass
        
last_key = None

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb, suppress = True)
keyboard.on_release_key("G", callback=kb_cb, suppress = True)

# Run this loop forever, or at least until someone presses control-C
while True:
    try:
        if last_key is not None:
            print("You pressed " + last_key)
            last_key = None

    except KeyboardInterrupt:
        break

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
            