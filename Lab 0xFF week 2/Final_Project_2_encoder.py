import pyb

class encoder:
    
    def __init__(self, pin1_in, pin2_in, timer, channel1, channel2, period):
        
        '''
        @brief              Create class to update motor position
        @param pin1_in      pin 1 of the encoder
        @param pin2_in      pin 2 of the encoder
        @param timer        counting the time of encoder
        @param period       period of the timer
        '''
        
        self.pin1_in = pin1_in
        self.pin2_in = pin2_in
        self.timer = timer
        self.channel1 = channel1
        self.channel2 = channel2
        self.period = 65536
        
        self.updatePos = 0      #Current position
        self.prevPos = 0        #Previous position
        self.deltaPos = 0       #Difference between the current and previous position
        self.totalPos = 0       #Total position the motor has rotated
        self.reset = 0          #Reset values
        
    def update(self):
        self.prevPos = self.updatePos
        self.updatePos = self.timer.counter() + self.reset
        
    def selectPos(self, reset):
        self.reset = reset
        if self.reset in range (0, 65536):
            self.updatePos = self.reset
            return self.updatePos
        else:
            print('please try again')
            
    def getDelta(self):
        self.deltaPos = abs(self.updatePos - self.prevPos)
        if self.deltaPos > (self.period+1)/2:
            if self.deltaPos > 0:
                self.deltaPos -= self.period+1
            else:
                self.deltaPos += self.period+1
            
    def takePos(self):
        print('your positions are: ')
        self.update()
        print('Previous position: ' + str(self.prevPos))
        print('Updated position: ' + str(self.updatePos))
        
if __name__ == '__main__':
    
    pin1 = pyb.Pin(pyb.Pin.cpu.B6)
    pin2 = pyb.Pin(pyb.Pin.cpu.B7)
    pin3 = pyb.Pin(pyb.Pin.cpu.C6)
    pin4 = pyb.Pin(pyb.Pin.cpu.C7)
    
    tim1 = pyb.Timer(4)
    tim2 = pyb.Timer(8)
    
    en1ch1 = tim1.channel(1, pin=pin1, mode=pyb.Timer.ENC_A)    #Encoder 1 channel 1
    en2ch1 = tim2.channel(1, pin=pin3, mode=pyb.Timer.ENC_B)    #Encoder 2 channel 1
    en1ch2 = tim1.channel(2, pin=pin2, mode=pyb.Timer.ENC_A)    #Encoder 1 channel 2
    en2ch2 = tim2.channel(2, pin=pin4, mode=pyb.Timer.ENC_B)    #Encoder 2 channel 2
    
    period1 = tim1.init(prescaler=1, period=65536)  #For encoder 1
    period2 = tim2.init(prescaler=1, period=65536)  #for encoder 2
    
    encoder1 = encoder(pin1, pin2, tim1, en1ch1, en1ch2, period1)
    encoder2 = encoder(pin3, pin4, tim2, en2ch1, en2ch2, period2)
